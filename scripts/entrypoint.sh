#!/bin/sh -e
PROFILE=${1:-"default"}
echo "Starting app..."

java -jar -Dspring.profiles.active=$PROFILE \
          app.jar